package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.exception.ValidateException;

public class Export extends FrontVerb{
	private static final long serialVersionUID = 1007425592831193320L;

	public Export(FrontDomain d) throws ValidateException{
		super(d);	
		this.denied = domain.isVerbDenied("Export");
		this.setVerbName("Export"+d.getCapFirstDomainName());
	}
	
	@Override
	public FrontMethod generateControllerMethod()  throws Exception {
		return null;
	}

	@Override
	public FrontMethod generateApiMethod()  throws Exception {
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}

}
