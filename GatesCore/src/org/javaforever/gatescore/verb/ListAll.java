package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.exception.ValidateException;

public class ListAll extends FrontVerb {
	private static final long serialVersionUID = -1934938999438010198L;
	
	public ListAll(FrontDomain d) throws ValidateException{
		super(d);
		this.denied = domain.isVerbDenied("ListAll");
		this.setVerbName("ListAll"+d.getCapFirstPlural());
	}

	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}

}
