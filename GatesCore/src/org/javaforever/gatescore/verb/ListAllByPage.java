package org.javaforever.gatescore.verb;

import org.javaforever.gatescore.core.FrontCodeBlock;
import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.core.FrontMethod;
import org.javaforever.gatescore.core.FrontVerb;
import org.javaforever.gatescore.exception.ValidateException;

public class ListAllByPage extends FrontVerb{
	private static final long serialVersionUID = -5590619489251226554L;

	public ListAllByPage(FrontDomain d) throws ValidateException{
		super(d);
		this.denied = domain.isVerbDenied("ListAllByPage");
		this.setVerbName("ListAll"+d.getCapFirstPlural()+"ByPage");
	}
	
	@Override
	public FrontMethod generateControllerMethod() throws Exception {
		return null;
	}

	@Override
	public FrontMethod generateApiMethod() throws Exception {
		return null;
	}

	@Override
	public FrontCodeBlock generateRouteBlockBlock() throws Exception {
		return null;
	}
	
}
