package org.javaforever.gatescore.vue;

import org.javaforever.gatescore.core.StatementList;

public abstract class ElementUIPage {
	public abstract StatementList generateStatementList() throws Exception;
	
	public StatementList generateTemplateHeaderStatementList() throws Exception{
		return null;
	}
	
	public StatementList genertateTemplateFooterStatementList()  throws Exception{
		return null;
	}
}
