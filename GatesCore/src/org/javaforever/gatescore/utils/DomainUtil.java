package org.javaforever.gatescore.utils;

import java.util.Set;

import org.javaforever.gatescore.core.FrontDomain;
import org.javaforever.gatescore.exception.ValidateException;

public class DomainUtil {
	public static FrontDomain findDomainInSet(Set<FrontDomain> targets, String domainName) throws ValidateException{
		for (FrontDomain d: targets){
			if (d.getStandardName().equals(domainName)) return d;
		}
		throw new ValidateException("域对象"+domainName+"不在列表中！");
	}
	
	public static Boolean inDomainSet(FrontDomain d,Set<FrontDomain> set){
		for (FrontDomain dn :set){
			if (d.getStandardName().equals(dn.getStandardName())){
				return true;
			}
		}
		return false;
	}
}
