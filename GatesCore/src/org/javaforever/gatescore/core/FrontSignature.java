package org.javaforever.gatescore.core;

import java.io.Serializable;

public class FrontSignature  implements Comparable<FrontSignature>,Cloneable,Serializable {
	private static final long serialVersionUID = 4164180928292136326L;
	protected int position;
	protected String signatureName;
	protected String signatureAlias;
	@Override
	public int compareTo(FrontSignature o) {
		return this.signatureName.compareTo(o.getSignatureName());
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getSignatureName() {
		return signatureName;
	}
	public void setSignatureName(String signatureName) {
		this.signatureName = signatureName;
	}
	public String getSignatureAlias() {
		return signatureAlias;
	}
	public void setSignatureAlias(String signatureAlias) {
		this.signatureAlias = signatureAlias;
	}
	
}
