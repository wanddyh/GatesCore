package org.javaforever.gatescore.core;

import java.io.Serializable;

public interface Writeable extends Serializable,Comparable<Writeable>{
	public Long getSerial();
	public String getContent();
	public String getContentWithSerial();
	public String getStatement(long pos);
	public String getStandardName();
}
